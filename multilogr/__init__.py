import logging
import logging.config
import sys


class BaseMultilogr(object):
    """Simplified logging setup for multiple loggers and handlers

    This should be subclassed to provide a custom logs spec

    Summary of some of the logging docs for quick reference:

        * The level number and level severity are directly related
        * ``... > WARNING > INFO > DEBUG > ...``
        * A logger message will be allowed if the message level >= logger level

    .. todo ::
        * Add more customization.  I originally hard-coded a bunch of this for
          a specific project.  I have made the most important parts
          customizable, but there is still a lot to convert.
        * Add formatters to logs spec.
        * Add file handlers customizations to log spec
    """

    #: dict[basestring, dict]: Log formatters to choose from the log spec
    FORMATTERS = dict(
        default=dict(
            format='%(name)s:%(asctime)s %(levelname)-8s %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S',
        ),
        compact=dict(
            format='%(name)s:%(asctime)s%(levelname)+8s %(message)s',
            datefmt='%Y%m%d-%H%M%S',
        ),
    )

    #: dict[basestring, dict]: Simplified config for multiple logs.
    #: Override with subclass.
    LOGS_SPEC = {}

    def __init__(self, logs_spec, formatters={}):
        self.logs_spec = logs_spec
        self.formatters = formatters if formatters else self.FORMATTERS

    @staticmethod
    def _level_num(level):
        """Return number corresponding to a :class:`logging` level

        If :attr:`level` is an :class:`int`, it will be passed through.
        :class:`None` indicates ignore all messages.

        Args:
            level (str|int|None): Level to translate

        Returns:
            int: The corresponding number
        """
        if isinstance(level, int):
            num = level
        elif level is None:
            num = logging.CRITICAL + 10
        else:
            num = getattr(logging, level)
        return num

    @classmethod
    def _config_logging(cls):
        # Add the basics for logging dict config
        d = {
            'version': 1,
            'formatters': cls.FORMATTERS,
            'handlers': {},
            'loggers': {
                # 'root': {
                #     'level': 'NOTSET',
                # },
            },
            'incremental': False,
        }

        # Add loggers and handlers to the config dict
        # according to the log spec
        for log_name, info in list(cls.LOGS_SPEC.items()):
            # Calculate handler level numbers
            level_nums = {k: cls._level_num(v) for k, v in info['levels'].items()}
            # The logger level is the least of the handler numbers
            level_nums['logger'] = sorted(level_nums.values())[0]

            # Configure file handler
            d['handlers']['file_' + log_name] = {
                'class': 'logging.handlers.RotatingFileHandler',
                'level': level_nums['file'],
                'formatter': 'default',
                # 10, 1 MB logs max
                'filename': info['log_path'],
                'mode': 'a',
                'maxBytes': 1048576,
                'backupCount': 10,
            }

            # Configure console handler
            d['handlers']['console_' + log_name] = {
                'class': 'logging.StreamHandler',
                'level': level_nums['console'],
                'formatter': 'compact',
                'stream': 'ext://sys.stderr',
            }

            # Configure logger
            d['loggers'][log_name] = dict(
                level=level_nums['logger'],
                handlers=('console_' + log_name, 'file_' + log_name),
                propagate=0,
            )

        # Apply the config to logging
        logging.config.dictConfig(d)

    @classmethod
    def get_logr(cls, name):
        """Similar to :meth:`logging.getLogger()`, but makes sure that logger is configured

        Args:
            name (basestring): Name of the logger

        Returns:
            logging.Logger: A configured logger matching `name`

        """
        logger = logging.getLogger(name)
        if not cls._is_configured(logger):
            cls._config_logging()
        logger = logging.getLogger(name)
        return logger

    @staticmethod
    def _has_handler(logger):
        """Returns `True` if `logger` has no configured handlers

        The handler :class:`logging.NullHandler` is not considered a configured
        handler.

        .. todo ::
            Support `propagate` similar to
            :class:`logging.Logger.hasHandlers()`

        Args:
            logger (logging.Logger): The logger to check

        Returns:
            bool:

        """
        if logger.handlers:
            return any(not isinstance(h, logging.NullHandler) for h in logger.handlers)
        else:
            return False

    @classmethod
    def _is_configured(cls, logger):
        return cls._has_handler(logger)


class LoggerWriter(object):
    def __init__(self, level, original=sys.__stderr__):
        # self.level is really like using log.debug(message)
        # at least in my case
        self.level = level
        self.original = original

    def write(self, message):
        # if statement reduces the amount of newlines that are
        # printed to the logger
        if message != '\n':
            self.level(message)

    def flush(self):
        # create a flush method so things can be flushed when
        # the system wants to. Not sure if simply 'printing'
        # sys.stderr is the correct way to do it, but it seemed
        # to work properly for me.
        return self.original.flush()

    def fileno(self):
        return self.original.fileno()
