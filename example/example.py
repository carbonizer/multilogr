from multilogr import BaseMultilogr


class Multilogr(BaseMultilogr):
    LOGS_SPEC = {
        'aaa': {
            'log_path': 'aaa.log',
            'levels': {
                'console': 'DEBUG',
                'file': 'DEBUG',
            },
        },
        'bbb': {
            'log_path': 'bbb.log',
            'levels': {
                'console': 'INFO',
                'file': 'DEBUG',
            },
        },
        'ccc': {
            'log_path': 'ccc.log',
            'levels': {
                'console': None,
                'file': 'INFO',
            },
        },
    }


def main():
    aaa_logr = Multilogr.get_logr('aaa')
    bbb_logr = Multilogr.get_logr('bbb')
    ccc_logr = Multilogr.get_logr('ccc')
    aaa_logr.debug('This should be on the console')
    aaa_logr.info('This should be on the console')
    bbb_logr.debug('This should NOT be on the console')
    bbb_logr.info('This should be on the console')
    ccc_logr.debug('This should NOT be on the console and NOT in the log')
    ccc_logr.info('This should NOT be on the console')

if __name__ == '__main__':
    main()
