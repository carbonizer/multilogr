multilogr
=========

Simplified logging setup for multiple loggers and handlers

Purpose
-------

``logging`` is a wonderful package, but it can be a pain to configure.  Basic
configuration takes a few lines of code.  That's fine for small programs, but
you often want more for larger projects.  The file-based configuration for
``logging`` is limited and would require a lot of redundant to code for
advanced setups.  ``multilogr`` simplifies some of these advanced setups.

The "multi" part of ``multilogr`` refers to two things:

    multiple loggers
        This is helpful for larger projects where you might want to use
        different loggers in different modules

    multiple handlers
        It is simple to configure both a log file handler as well as a handler
        to stream to stderr

Installation
------------

For regular use::

    pip install git+https://bitbucket.org/carbonizer/multilogr

If develop with it or play with the example::

    pip install -e git+https://bitbucket.org/carbonizer/multilogr

or probably more common (so you don't have to go searching for the repo) is
something like::

    git clone https://bitbucket.org/carbonizer/multilogr
    pip install -e ./multilogr

Configuration
-------------

Subclass ``multilogr.BaseMultilogr`` and override ``LOGS_SPEC`` with your own
logs specification as explained next.

logs specification
``````````````````

The logs spec is a dictionary of logger specs, the keys of which are the names of the loggers to
be configured.

logger specification
````````````````````

The logger spec is a dictionary

    *log_path*
        Path to the output log file for use with the ``RotatingFileHandler``

    *levels*
        Dictionary of ``logging`` level

levels dictionary
`````````````````

*levels* is a dictionary of ``logging`` levels.  The values can be strings or
ints as described in the ``logging`` docs, or ``None``.  ``None`` indicates that the
handler should be used.  This make it easy to enable logging to the console
while debugging, then disable it for production.  The keys of the dictionary
refer to the following handlers:

    *console*
        level for ``StreamHandler`` to ``stderr``

    *file*
        level for ``RotatingFileHandler`` to *log_path*

Example logs spec
`````````````````

::

    LOGS_SPEC = {
        'aaa': {
            'log_path': 'aaa.log',
            'levels': {
                'console': 'DEBUG',
                'file': 'DEBUG',
            },
        },
        'bbb': {
            'log_path': 'bbb.log',
            'levels': {
                'console': 'INFO',
                'file': 'DEBUG',
            },
        },
        'ccc': {
            'log_path': 'ccc.log',
            'levels': {
                'console': None,
                'file': 'INFO',
            },
        },
    }

Example
-------

An example script is include with the project to help illustrate usage.

TODOs
-----

* Add more customization.  I originally hard-coded a bunch of this for a
  specific project.  I have made the most important parts customizable, but
  there is still a lot to convert.

* Add formatters to logs spec.

* Add file handlers customizations to log spec

* Better docs than just a README
